# Ghost in the Tor docker image
# Example usage with docker-compose
### 1. Create tor directory
```
mkdir tor
sudo chown -R 101:101 tor
```
### 2. Create a docker-compose.yml with the following content
```yaml
version: "3"
services:
  tor:
    image: thetorproject/obfs4-bridge
    container_name: tor
    volumes:
       - ./tor:/var/lib/tor
       - ./torrc:/etc/tor/torrc
    restart: always

  ghost:
    image: ghost
    container_name: ghost
    restart: always
    expose:
      - "2368"
    # ports:
    #   - 2368:2368

    environment:
      # see https://ghost.org/docs/config/#configuration-options
      database__client: mysql
      database__connection__host: db
      database__connection__user: root
      database__connection__password: passwd_change_me_va7chiDo
      database__connection__database: ghost
      # this url value is just an example, and is likely wrong for your environment!
      url: http://underground.io
      # contrary to the default mentioned in the linked documentation, this image defaults to NODE_ENV=production (so development mode needs to be explicitly specified if desired)
      #NODE_ENV: development

  db:
    image: mysql
    container_name: mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: passwd_change_me_va7chiDo
```
### 3. Create the tor config
- torrc documentation https://2019.www.torproject.org/docs/tor-manual.html.en

```
$ nano ./torrc
HiddenServiceDir /var/lib/tor/hs/
HiddenServicePort 80 ghost:2368
```

### 4. Start docker-compose
```
sudo docker-compose up -d
```

### 5. Get the onion address (hostname)
```
sudo cat ./tor/hs/hostname
```
Output: youronionaddress.onion

### 6. Access Ghost over Tor
Download and install Tor Browser from https://www.torproject.org/download/

Now Ghost can be accessed at http://youronionaddress.onion from any Tor Browser.

### 7. Be patient
It takes a while for information to spread through the network. Even 2 days may not be a long time.

### 8. Prepare the site
In the meantime, you can read the logs from the tor container or prepare your new pages. You can uncomment ports and connect to http://localhost:2368/ghost - but only allow this access from your own local network.

### 9. Ghost documentation
Study the [Ghost documentation](https://ghost.org/docs/publishing/), at least for publishing new posts. 


Thanks to [lu4p](https://github.com/lu4p/tor-docker)